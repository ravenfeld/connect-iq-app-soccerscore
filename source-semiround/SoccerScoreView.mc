using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Time.Gregorian as Gregorian;

class SoccerScoreView extends Ui.View {

	hidden var halfLength;
	hidden var halftime;
	hidden var press;
	hidden var start;
	hidden var first;
	hidden var second;
	hidden var half;
	hidden var extraHalf;
	hidden var extraTime;
	hidden var needed;
	hidden var home;
	hidden var guest;
	hidden var total;
	hidden var timeout;
	hidden var end;
	hidden var fontLarge;
	hidden var fontMedium;
    function initialize() {
        View.initialize();
    }

    // Load your resources here
    function onLayout(dc) {
    	fontLarge =  Ui.loadResource(Rez.Fonts.FontLarge);
    	fontMedium =  Ui.loadResource(Rez.Fonts.FontMedium);
        halfLength = Ui.loadResource(Rez.Strings.HalfLengthScreen);
        halftime = Ui.loadResource(Rez.Strings.Halftime);
        press = Ui.loadResource(Rez.Strings.Press);
        start = Ui.loadResource(Rez.Strings.Start);
     	first = Ui.loadResource(Rez.Strings.First);
     	second = Ui.loadResource(Rez.Strings.Second);
     	half = Ui.loadResource(Rez.Strings.Half);
     	extraHalf = Ui.loadResource(Rez.Strings.ExtraHalf);
     	extraTime = Ui.loadResource(Rez.Strings.ExtraTimeScreen);
     	needed = Ui.loadResource(Rez.Strings.Needed);
     	home = Ui.loadResource(Rez.Strings.Home);
     	guest = Ui.loadResource(Rez.Strings.Guest);
     	total = Ui.loadResource(Rez.Strings.Total);
     	timeout = Ui.loadResource(Rez.Strings.Timeout);
     	end = Ui.loadResource(Rez.Strings.End);
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
    		
    	
    	var moment = Time.now();
        var date = Gregorian.info(moment, Time.FORMAT_LONG);
        
        // Call the parent onUpdate function to redraw the layout
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_WHITE);
        dc.clear();
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        if(model.status==:Prep){
        	dc.drawText(dc.getWidth()/2, dc.getHeight()*0.01, Gfx.FONT_TINY, press, Gfx.TEXT_JUSTIFY_CENTER);
        	dc.drawText(dc.getWidth()/2, dc.getHeight()*0.09, Gfx.FONT_TINY, start, Gfx.TEXT_JUSTIFY_CENTER);
        }else if(model.period==:FirstHalf){
        	dc.drawText(dc.getWidth()/2, dc.getHeight()*0.01, Gfx.FONT_TINY, first, Gfx.TEXT_JUSTIFY_CENTER);
        	if(model.isExtraTime){
        		dc.drawText(dc.getWidth()/2, dc.getHeight()*0.09, Gfx.FONT_TINY, extraHalf, Gfx.TEXT_JUSTIFY_CENTER);
        	}else{
        		dc.drawText(dc.getWidth()/2, dc.getHeight()*0.09, Gfx.FONT_TINY, half, Gfx.TEXT_JUSTIFY_CENTER);
        	}
        }else if(model.period==:Halftime){
        	dc.drawText(dc.getWidth()/2, dc.getHeight()*0.075, Gfx.FONT_TINY, halftime, Gfx.TEXT_JUSTIFY_CENTER);
        }else if(model.period==:SecondHalf && model.display!=:ExtraTimeNeed && model.display!=:Stop_ExtraTimeNeed){
       	 	dc.drawText(dc.getWidth()/2, dc.getHeight()*0.01, Gfx.FONT_TINY, second, Gfx.TEXT_JUSTIFY_CENTER);
       	 	if(model.isExtraTime){
        		dc.drawText(dc.getWidth()/2, dc.getHeight()*0.09, Gfx.FONT_TINY, extraHalf, Gfx.TEXT_JUSTIFY_CENTER);
        	}else{
        		dc.drawText(dc.getWidth()/2, dc.getHeight()*0.09, Gfx.FONT_TINY, half, Gfx.TEXT_JUSTIFY_CENTER);
        	}
        }else if(model.display==:ExtraTimeNeed|| model.display==:Stop_ExtraTimeNeed){
        	dc.drawText(dc.getWidth()/2, dc.getHeight()*0.01, Gfx.FONT_TINY, extraTime, Gfx.TEXT_JUSTIFY_CENTER);
        	dc.drawText(dc.getWidth()/2, dc.getHeight()*0.09, Gfx.FONT_TINY, needed, Gfx.TEXT_JUSTIFY_CENTER);
        }else if(model.display==:Stop){
        	dc.drawText(dc.getWidth()/2, dc.getHeight()*0.075, Gfx.FONT_TINY, end, Gfx.TEXT_JUSTIFY_CENTER);
        }
        
        dc.drawText(dc.getWidth()/2, dc.getHeight()*0.21, Gfx.FONT_TINY, ":", Gfx.TEXT_JUSTIFY_CENTER);
        
        var text_scoreHome = dc.getTextWidthInPixels(model.scoreHome.toString(),fontMedium);
        var text_scoreGuest = dc.getTextWidthInPixels(model.scoreGuest.toString(),fontMedium);
        dc.drawText(dc.getWidth()/2-12-text_scoreHome, dc.getHeight()*0.26, Gfx.FONT_TINY, home, Gfx.TEXT_JUSTIFY_RIGHT|Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(dc.getWidth()/2-12, dc.getHeight()*0.27, fontMedium, model.scoreHome, Gfx.TEXT_JUSTIFY_RIGHT|Gfx.TEXT_JUSTIFY_VCENTER);
        
        dc.drawText(dc.getWidth()/2+12+text_scoreGuest, dc.getHeight()*0.26, Gfx.FONT_TINY, guest, Gfx.TEXT_JUSTIFY_LEFT|Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(dc.getWidth()/2+12, dc.getHeight()*0.27,fontMedium, model.scoreGuest, Gfx.TEXT_JUSTIFY_LEFT|Gfx.TEXT_JUSTIFY_VCENTER);
        
        dc.drawText(dc.getWidth()/2-12, dc.getHeight()*0.4, Gfx.FONT_TINY, halfLength, Gfx.TEXT_JUSTIFY_RIGHT|Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(dc.getWidth()/2+12, dc.getHeight()*0.4, Gfx.FONT_TINY, total, Gfx.TEXT_JUSTIFY_LEFT|Gfx.TEXT_JUSTIFY_VCENTER);
     	
     	if(model.halfLength>=36000){
     		dc.drawText(dc.getWidth()/2-12, dc.getHeight()*0.57, fontMedium, Utils.timeToString(model.halfLength), Gfx.TEXT_JUSTIFY_RIGHT|Gfx.TEXT_JUSTIFY_VCENTER);
     	}else{
     	    dc.drawText(dc.getWidth()/2-12, dc.getHeight()*0.57, fontLarge, Utils.timeToString(model.halfLength), Gfx.TEXT_JUSTIFY_RIGHT|Gfx.TEXT_JUSTIFY_VCENTER);        
     	}
     	
     	if(model.total>=36000){
        	dc.drawText(dc.getWidth()/2+12, dc.getHeight()*0.57, fontMedium, Utils.timeToString(model.total), Gfx.TEXT_JUSTIFY_LEFT|Gfx.TEXT_JUSTIFY_VCENTER);
        }else{
        	dc.drawText(dc.getWidth()/2+12, dc.getHeight()*0.57, fontLarge, Utils.timeToString(model.total), Gfx.TEXT_JUSTIFY_LEFT|Gfx.TEXT_JUSTIFY_VCENTER);        
        }
        dc.drawText(dc.getWidth()/2-12, dc.getHeight()*0.73, Gfx.FONT_TINY, timeout, Gfx.TEXT_JUSTIFY_RIGHT|Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(dc.getWidth()/2+12, dc.getHeight()*0.73, Gfx.FONT_TINY, halftime, Gfx.TEXT_JUSTIFY_LEFT|Gfx.TEXT_JUSTIFY_VCENTER);
        if(model.timeout>0){
        	dc.setColor(Gfx.COLOR_BLUE, Gfx.COLOR_TRANSPARENT);
        }else{
        	dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        }
        dc.drawText(dc.getWidth()/2-12, dc.getHeight()*0.82, Gfx.FONT_SMALL, "+"+Utils.timeToString(model.timeout), Gfx.TEXT_JUSTIFY_RIGHT|Gfx.TEXT_JUSTIFY_VCENTER);
        
        if(model.period==:Halftime){
        	dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
        }else{
        	dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        }
        dc.drawText(dc.getWidth()/2+12, dc.getHeight()*0.82, Gfx.FONT_SMALL, Utils.timeToString(model.halftime), Gfx.TEXT_JUSTIFY_LEFT|Gfx.TEXT_JUSTIFY_VCENTER);
        
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
       
        var hourString = date.hour.format("%02d"); 
        var minuteString = date.min.format("%02d");
        var secondString = date.sec.format("%02d");
        dc.drawText(dc.getWidth()/2, dc.getHeight()-dc.getHeight()*0.125, Gfx.FONT_TINY,hourString+":"+minuteString+":"+secondString, Gfx.TEXT_JUSTIFY_CENTER);
   
   		var width = dc.getWidth()-dc.getWidth()*0.225*2;
 		var firstHalfMax = model.halfLengthMax*width/(model.halfLengthMax*2+model.halftimeMax);
   		var halftimeMax = (model.halfLengthMax+model.halftimeMax)*width/(model.halfLengthMax*2+model.halftimeMax);
   		
   		if(model.period==:FirstHalf && model.halfLength>0 ){
         	var firstHalf =(model.halfLengthMax- model.halfLength)*width/(model.halfLengthMax*2+model.halftimeMax);
       		dc.setPenWidth(5);
			dc.setColor(Gfx.COLOR_GREEN, Gfx.COLOR_TRANSPARENT);
       		dc.drawLine(firstHalf+dc.getWidth()*0.225,dc.getHeight()*0.01, firstHalfMax+dc.getWidth()*0.225,dc.getHeight()*0.01);
       		dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
       		dc.drawLine(firstHalfMax+dc.getWidth()*0.225,dc.getHeight()*0.01, halftimeMax+dc.getWidth()*0.225,dc.getHeight()*0.01);
       		dc.setColor(Gfx.COLOR_GREEN, Gfx.COLOR_TRANSPARENT);
       		dc.drawLine(halftimeMax+dc.getWidth()*0.225,dc.getHeight()*0.01, dc.getWidth()-dc.getWidth()*0.225,dc.getHeight()*0.01);
   		}else if( (model.period==:FirstHalf && model.halfLength==0) || (model.period==:Halftime && model.halftime>0)){
   			var halfTime =(model.halfLengthMax+model.halftimeMax - model.halftime)*width/(model.halfLengthMax*2+model.halftimeMax);
       		dc.setPenWidth(5);
       		dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
       		dc.drawLine(halfTime+dc.getWidth()*0.225,dc.getHeight()*0.01, halftimeMax+dc.getWidth()*0.225,dc.getHeight()*0.01);
       		dc.setColor(Gfx.COLOR_GREEN, Gfx.COLOR_TRANSPARENT);
       		dc.drawLine(halftimeMax+dc.getWidth()*0.225,dc.getHeight()*0.01, dc.getWidth()-dc.getWidth()*0.225,dc.getHeight()*0.01);
   		}else if ( (model.period==:Halftime && model.halftime==0) || (model.period==:SecondHalf && model.halfLength>0)){
   			var secondHalf =(model.halfLengthMax*2+model.halftimeMax - model.halfLength)*width/(model.halfLengthMax*2+model.halftimeMax);
       		dc.setPenWidth(5);  
       		dc.setColor(Gfx.COLOR_GREEN, Gfx.COLOR_TRANSPARENT);
       		dc.drawLine(secondHalf+dc.getWidth()*0.225,dc.getHeight()*0.01, dc.getWidth()-dc.getWidth()*0.225,dc.getHeight()*0.01);
       	}
               		
	    if(model.display==:Start){
	        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        	dc.setPenWidth(4);
        	dc.drawLine(dc.getWidth()/2-30-2,dc.getHeight()/2-40-2,dc.getWidth()/2+50+4,dc.getHeight()/2);
        	dc.drawLine(dc.getWidth()/2+50+4,dc.getHeight()/2,dc.getWidth()/2-30-2,dc.getHeight()/2+40+4);
        	dc.drawLine(dc.getWidth()/2-30-2,dc.getHeight()/2-40-2,dc.getWidth()/2-30-2,dc.getHeight()/2+40+4);
    		var triangle = [ [dc.getWidth()/2-30,dc.getHeight()/2-40], [dc.getWidth()/2+50,dc.getHeight()/2],[dc.getWidth()/2-30,dc.getHeight()/2+40] ];
    		dc.setColor(Gfx.COLOR_GREEN, Gfx.COLOR_TRANSPARENT);
        	dc.fillPolygon(triangle);
        	dc.setPenWidth(7);
        	dc.drawCircle(dc.getWidth()/2,dc.getHeight()/2,dc.getWidth()/2);  
        	
	    }else if(model.display==:Stop || model.display==:Stop_ExtraTimeNeed){
        	var size = 80;
        	dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        	dc.setPenWidth(4);
        	dc.drawRectangle(dc.getWidth()/2-size/2-2, dc.getHeight()/2-size/2-2, size+4, size+4);
        	dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
        	dc.fillRectangle(dc.getWidth()/2-size/2, dc.getHeight()/2-size/2, size, size);
        	dc.setPenWidth(7);
        	dc.drawCircle(dc.getWidth()/2,dc.getHeight()/2,dc.getWidth()/2); 
        }else if(model.display==:Pause){
        	var size = 80;
        	
        	dc.setColor(Gfx.COLOR_BLUE, Gfx.COLOR_TRANSPARENT);
        	dc.fillRectangle(dc.getWidth()/2-size/2, dc.getHeight()/2-size/2, size/2-8, size);
        	dc.fillRectangle(dc.getWidth()/2+6, dc.getHeight()/2-size/2, size/2-8, size);
        	dc.setPenWidth(7);
        	dc.drawCircle(dc.getWidth()/2,dc.getHeight()/2,dc.getWidth()/2); 
        	
        	dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        	dc.setPenWidth(4);
        	dc.drawRectangle(dc.getWidth()/2-size/2-2, dc.getHeight()/2-size/2-2, size/2-4, size+4);
        	dc.drawRectangle(dc.getWidth()/2+4, dc.getHeight()/2-size/2-2, size/2-4, size+4);
        }
        
        if(model.status==:Pause){
        	dc.setPenWidth(7);
        	dc.setColor(Gfx.COLOR_BLUE, Gfx.COLOR_TRANSPARENT);
        	dc.drawCircle(dc.getWidth()/2,dc.getHeight()/2,dc.getWidth()/2); 
        }
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }
}
