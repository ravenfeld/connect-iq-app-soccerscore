using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class TimeMenuItem extends MenuItem{

	hidden var property;
	
	function initialize (id, value,property ) {
		MenuItem.initialize(id, value);
		self.property=property;
	}
	
	function onShow () {
		MenuItem.setValue(Utils.timeToString(App.getApp().getProperty(property))); 
	}
}