using Toybox.WatchUi as Ui;
using Toybox.Timer as Timer;
using Toybox.Application as App;


class SoccerScoreModel{
	
	var status = :Prep;
	var display = :None;
	var period = :FirstHalf;
	var isExtraTime=false;
	hidden var extraTime=false;
	var scoreHome;
	var scoreGuest;
	var halfLength;
	var total;
	var timeout;
	var halftime;
	var halfLengthMax;
	var halftimeMax;	
	hidden var halfLengthInit;
	hidden var halftimeInit;
	hidden var halfLengthExtraTimeInit;
	hidden var halftimeExtraTimeInit;
	hidden var refreshTimer = new Timer.Timer();
	hidden var displayTimer = new Timer.Timer();
	hidden var buzzTimer = new Timer.Timer();
	
	function initialize(){
		newMatch();
		refreshTimer.start(method(:refresh), 1000, true);
	}
	
	function newMatch(){
		displayTimer.stop();
		buzzTimer.stop();
		status = :Prep;
	 	display = :None;
		period = :FirstHalf;
		scoreHome = 0;
		scoreGuest = 0;
		halfLengthInit = App.getApp().getProperty("halfLength");
		halftimeInit = App.getApp().getProperty("halftime");
		halfLengthExtraTimeInit = App.getApp().getProperty("extraTime_halfLength");
		halftimeExtraTimeInit = App.getApp().getProperty("extraTime_halftime");
		extraTime = App.getApp().getProperty("extraTime");
		total = 0;
		timeout = 0;
		isExtraTime=false;
		halfLength = halfLengthInit;
		halftime = halftimeInit;
		halfLengthMax=halfLengthInit;
		halftimeMax=halftimeInit;
		Ui.requestUpdate();
	}
	
	function refresh(){
		if(status==:Start){
			if((period==:FirstHalf || period==:SecondHalf) && halfLength>0){
				halfLength = halfLength-1;
				if(halfLength==0){
					startBuzz();
					if(extraTime && period==:SecondHalf && !isExtraTime && scoreHome==scoreGuest){
						display=:ExtraTimeNeed;
					}
				}
			}else if(period==:Halftime && halftime>0){
				halftime = halftime - 1;
				if(halftime==0){
					startBuzz();
				}
			}
			total = total+1;
		}else if(status==:Pause){
			timeout = timeout+1;
			total = total+1;
		}
		Ui.requestUpdate();
	}
	
	function startFirstHalf(){
		buzzTimer.stop();
		displayTimer.stop();
		period=:FirstHalf;
		if(isExtraTime){
			total=halfLengthInit*2;
		}else{
			total=0;
		}
		timeout=0;
		status=:Start;
		display=:Start;
		startBuzz();
		displayTimer.start(method(:resetDisplay), 2000, false);
		Ui.requestUpdate();
	}
	
	function startHalftime(){
		period=:Halftime;
		halftime = halftimeMax;
		total=0;
		halfLength=halfLengthMax;
		timeout=0;
		display=:Start;
		startBuzz();
		displayTimer.start(method(:resetDisplay), 2000, false);
		Ui.requestUpdate();
	}
	
	function startSecondHalf(){
		period=:SecondHalf;
		if(isExtraTime){
			total=halfLengthInit*2+halfLengthExtraTimeInit;
		}else{
			total=halfLengthInit;
		}
		halfLength=halfLengthMax;
		timeout=0;
		display=:Start;
		startBuzz();
		displayTimer.start(method(:resetDisplay), 2000, false);
		Ui.requestUpdate();
	}
		
	function continueMatch(){
		buzzTimer.stop();
		displayTimer.stop();
		status=:Start;
		display=:Start;
		startBuzz();
		displayTimer.start(method(:resetDisplay), 2000, false);
		Ui.requestUpdate();
	}
			
	function pauseMatch(){
		status=:Pause;
		display=:Pause;
		buzzTimer.stop();
		displayTimer.stop();
		startBuzz();
		buzzTimer.start(method(:startBuzz), 10000, true);
		displayTimer.start(method(:resetDisplay), 2000, false);
		Ui.requestUpdate();
	}
	
	function stopMatch(backToMenu){
		status=:Stop;
		display=:Stop;
		displayTimer.stop();
		buzzTimer.stop();
		if(backToMenu){
			displayTimer.start(method(:displayMenu), 6000, false);
		}else{
			displayTimer.start(method(:displayMenu), 2000, false);
			startBuzz();
		}
		buzzTimer.start(method(:startBuzz), 10000, true);
		Ui.requestUpdate();
	}
	
	function endMatch(backToMenu){
		status=:Stop;
		period=:End;
		displayTimer.stop();
		buzzTimer.stop();
		if(display==:ExtraTimeNeed || display==:Stop_ExtraTimeNeed){
			display=:Stop_ExtraTimeNeed;
		}else{
			display=:Stop;
		}
		if(backToMenu){
			displayTimer.start(method(:displayMenu), 6000, false);
		}else{
			displayTimer.start(method(:displayMenu), 2000, false);
			startBuzz();
		}
		Ui.requestUpdate();
	}
		
	function startExtraTime(){
		isExtraTime=true;
		halfLength = halfLengthExtraTimeInit;
		halftime = halftimeExtraTimeInit;
		halfLengthMax=halfLength;
		halftimeMax=halftime;
		startFirstHalf();
	}
	
	function addScoreHome(){
		if(status!=:Prep && period!=:Halftime){
			scoreHome=scoreHome+1;
			Ui.requestUpdate();
		}
	}
	
	function addScoreGuest(){
		if(status!=:Prep && period!=:Halftime){
			scoreGuest=scoreGuest+1;
			Ui.requestUpdate();
		}
	}
	
	function pullScoreHome(){
		if(status!=:Prep){
			scoreHome=scoreHome-1;
			Ui.requestUpdate();
		}
	}
	
	function pullScoreGuest(){
		if(status!=:Prep){
			scoreGuest=scoreGuest-1;
			Ui.requestUpdate();
		}
	}
	
	function startBuzz() {
		beep();
		vibrate(1500);
	}
	
	function displayMenu(){
		var menuItems = new [0];
		if(period==:End && (display==:ExtraTimeNeed || display==:Stop_ExtraTimeNeed)){
			menuItems.add(new MenuItem (:ExtraTime,Ui.loadResource(Rez.Strings.ExtraTime)));
			menuItems.add(new MenuItem (:Exit,Ui.loadResource(Rez.Strings.Exit)));
			menuItems.add(new MenuItem (:NewMatch,Ui.loadResource(Rez.Strings.NewMatch)));
		}else if(period==:End){
			menuItems.add(new MenuItem (:Exit,Ui.loadResource(Rez.Strings.Exit)));
			menuItems.add(new MenuItem (:NewMatch,Ui.loadResource(Rez.Strings.NewMatch)));
		}else{
			menuItems.add(new MenuItem (:Continue,Ui.loadResource(Rez.Strings.Continue)));
			if(scoreHome>0){
				menuItems.add(new MenuItem (:PullScoreHome,Ui.loadResource(Rez.Strings.PullScoreHome)));
			}
			if(scoreGuest>0){
				menuItems.add(new MenuItem (:PullScoreGuest,Ui.loadResource(Rez.Strings.PullScoreGuest)));
			}
			menuItems.add(new MenuItem (:NewMatch,Ui.loadResource(Rez.Strings.NewMatch)));
			menuItems.add(new MenuItem (:Exit,Ui.loadResource(Rez.Strings.Exit)));
		}
		var menu = new Menu (menuItems, model.scoreHome+ " : "+model.scoreGuest);
		Ui.pushView(menu, new PauseEndMenuDelegate(menu),  Ui.SLIDE_LEFT );
		resetDisplay();
	}
	
	function vibrate(duration) {
		if(Attention has :vibrate && App.getApp().getProperty("vibrate")){
			var vibrateData = [ new Attention.VibeProfile(  100, duration ) ];
			Attention.vibrate( vibrateData );
		}
	}

	function beep() {
		if( App.getApp().getProperty("beep") && Attention has :playTone){
			Attention.playTone(Attention.TONE_TIME_ALERT);
		}
	}
	
	function stop(){
		refreshTimer.stop();
		displayTimer.stop();
		buzzTimer.stop();
	}
	
	function resetDisplay(){
		if(display!=:ExtraTimeNeed && display!=:Stop_ExtraTimeNeed){
			display=:None;
		}
	}
}