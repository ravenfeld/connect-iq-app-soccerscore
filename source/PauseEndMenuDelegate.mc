using Toybox.WatchUi as Ui;
using Toybox.System as Sys;

class PauseEndMenuDelegate extends MenuDelegate {
	
    function initialize(menu) {
        MenuDelegate.initialize(menu);
    }

    function onMenuItem(item) {
 		if (item.id == :Continue) {
            model.continueMatch();
            Ui.popView (Ui.SLIDE_RIGHT);
		}else if (item.id == :Exit) {
			model.stop();
            Toybox.System.exit();
        }else if (item.id == :NewMatch) {
            model.newMatch();
            Ui.popView (Ui.SLIDE_RIGHT);
        }else if (item.id == :ExtraTime) {
            model.startExtraTime();
            Ui.popView (Ui.SLIDE_RIGHT);
        }else if (item.id == :PullScoreHome) {
            model.pullScoreHome();
            onBack();
        }else if (item.id == :PullScoreGuest) {
            model.pullScoreGuest();
            onBack();
        }
    }
    
    function onBack(){
    	if(model.period==:End){
    		model.endMatch(true);
    	}else{
    		model.stopMatch(true);
    	}
		Ui.popView (Ui.SLIDE_RIGHT);
		return true;
	}
}