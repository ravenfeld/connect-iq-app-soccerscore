using Toybox.WatchUi as Ui;
using Toybox.Application as App;

class SoccerScoreDelegate extends Ui.BehaviorDelegate {

	hidden var halfLength;
	hidden var halftime;
	hidden var extraTime;
	hidden var extraTimeHalfLength;
	hidden var extraTimeHalftime;	
	hidden var alarms;
	hidden var bip;
	hidden var vibrate;
	hidden var settings;
	
    function initialize() {
        BehaviorDelegate.initialize();
        settings = Ui.loadResource(Rez.Strings.Settings);
        halfLength = Ui.loadResource(Rez.Strings.HalfLength);
        halftime = Ui.loadResource(Rez.Strings.Halftime);
        extraTime = Ui.loadResource(Rez.Strings.ExtraTime);
        extraTimeHalfLength = Ui.loadResource(Rez.Strings.ExtraTimeHalfLength);
        extraTimeHalftime = Ui.loadResource(Rez.Strings.ExtraTimeHalftime);
        alarms = Ui.loadResource(Rez.Strings.Alarms);
        bip = Ui.loadResource(Rez.Strings.Bip);
        vibrate = Ui.loadResource(Rez.Strings.Vibrate);
    }
    
    function onMenu() {
    	if (model.status == :Prep) {
    		var menuItems = new [0];
    		if(App.getApp().getProperty("extraTime")){
    			menuItems.add(new TimeMenuItem (:HalfLength, halfLength, "halfLength"));
        		menuItems.add(new TimeMenuItem (:Halftime,halftime,"halftime"));
        		menuItems.add(new StateMenuItem (:ExtraTime,extraTime,"extraTime"));
        		menuItems.add(new TimeMenuItem (:ExtraTimeHalfLength, extraTimeHalfLength, "extraTime_halfLength"));
        		menuItems.add(new TimeMenuItem (:ExtraTimeHalftime,extraTimeHalftime,"extraTime_halftime"));
	    		menuItems.add(new MenuItem (:Alarms,alarms));
    		}else{
    			menuItems.add(new TimeMenuItem (:HalfLength, halfLength, "halfLength"));
        		menuItems.add(new TimeMenuItem (:Halftime,halftime,"halftime"));
        		menuItems.add( new StateMenuItem (:ExtraTime,extraTime,"extraTime"));
	    		menuItems.add(new MenuItem (:Alarms,alarms));
        	}
    		var menu = new Menu (menuItems, settings);        	
        	Ui.pushView(menu, new EditMenuDelegate(menu), WatchUi.SLIDE_RIGHT);
        }else{
        	Ui.pushView(new ErrorView(Ui.loadResource(Rez.Strings.Error)), null, WatchUi.SLIDE_RIGHT);
        }
        return true;
    }

	function onSelect() {
		if (model.status == :Prep) {
			model.startFirstHalf();
		}else if(model.status == :Stop){
			model.continueMatch();
		}else {
			if(model.period==:FirstHalf && model.halfLength==0){
				model.startHalftime();
			}else if(model.period==:Halftime){
				model.startSecondHalf();
			}else if(model.period==:SecondHalf && model.halfLength==0){
				model.endMatch(false);
			}else{
				model.stopMatch(false);
			}
		}
	}
	
	function onBack() {
		if(model.status !=:Prep && model.status !=:Stop && model.status!=:Stop_ExtraTimeNeed){
			if (model.status == :Pause) {
				model.continueMatch();
			}else {
				model.pauseMatch();
			}
			return true;
		}	
	}
	
	function onNextPage () {
		model.addScoreGuest();
		return true;
	}
	
	function onPreviousPage () {
		model.addScoreHome();
		return true;		
	}
}