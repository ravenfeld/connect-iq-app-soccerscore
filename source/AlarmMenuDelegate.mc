using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Application as App;

class AlarmMenuDelegate extends MenuDelegate {
		
    function initialize(menu) {
        MenuDelegate.initialize(menu);
    }
	
    function onMenuItem(item) {
		if(item.id ==:Beep){
    		if(App.getApp().getProperty("beep")){
    			App.getApp().setProperty("beep",false);
    		}else{
    			App.getApp().setProperty("beep",true);
    		}
    	}else if(item.id ==:Vibrate){
    		if(App.getApp().getProperty("vibrate")){
    			App.getApp().setProperty("vibrate",false);
    		}else{
    			App.getApp().setProperty("vibrate",true);
    		}
    	}
    }
    
    function onBack(){
		Ui.popView (Ui.SLIDE_RIGHT);
		return true;
	}
}