using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Application as App;

class EditMenuDelegate extends MenuDelegate {
	
	hidden var halfLength;
	hidden var halftime;
	hidden var extraTime;
	hidden var extraTimeHalfLength;
	hidden var extraTimeHalftime;
	hidden var alarms;
	hidden var bip;
	hidden var vibrate;
	
    function initialize(menu) {
        MenuDelegate.initialize(menu);
        halfLength = Ui.loadResource(Rez.Strings.HalfLength);
        halftime = Ui.loadResource(Rez.Strings.Halftime);
        extraTime = Ui.loadResource(Rez.Strings.ExtraTime);
        extraTimeHalfLength = Ui.loadResource(Rez.Strings.ExtraTimeHalfLength);
        extraTimeHalftime = Ui.loadResource(Rez.Strings.ExtraTimeHalftime);
        alarms = Ui.loadResource(Rez.Strings.Alarms);
        bip = Ui.loadResource(Rez.Strings.Bip);
        vibrate = Ui.loadResource(Rez.Strings.Vibrate);
    }

    function onMenuItem(item) {
    	if(item.id == :HalfLength){
    		Ui.pushView(new TimePicker(halfLength,"halfLength"), new TimePickerDelegate("halfLength"), Ui.SLIDE_LEFT);
    	}else if(item.id == :Halftime){
    		Ui.pushView(new TimePicker(halftime,"halftime"), new TimePickerDelegate("halftime"), Ui.SLIDE_LEFT);
    	}else if(item.id == :ExtraTimeHalfLength){
    		Ui.pushView(new TimePicker(extraTimeHalfLength,"extraTime_halfLength"), new TimePickerDelegate("extraTime_halfLength"), Ui.SLIDE_LEFT);
    	}else if(item.id == :ExtraTimeHalftime){
    		Ui.pushView(new TimePicker(extraTimeHalftime,"extraTime_halftime"), new TimePickerDelegate("extraTime_halftime"), Ui.SLIDE_LEFT);
    	}else if(item.id ==:ExtraTime){
    		var menuItems = new [0];
    		if(App.getApp().getProperty("extraTime")){
    			App.getApp().setProperty("extraTime",false);
        		menuItems.add(new TimeMenuItem (:HalfLength, halfLength, "halfLength"));
        		menuItems.add(new TimeMenuItem (:Halftime,halftime,"halftime"));
        		menuItems.add( new StateMenuItem (:ExtraTime,extraTime,"extraTime"));
	    		menuItems.add(new MenuItem (:Alarms,alarms));
    		}else{
    			App.getApp().setProperty("extraTime",true);
        		menuItems.add(new TimeMenuItem (:HalfLength, halfLength, "halfLength"));
        		menuItems.add(new TimeMenuItem (:Halftime,halftime,"halftime"));
        		menuItems.add(new StateMenuItem (:ExtraTime,extraTime,"extraTime"));
        		menuItems.add(new TimeMenuItem (:ExtraTimeHalfLength, extraTimeHalfLength, "extraTime_halfLength"));
        		menuItems.add(new TimeMenuItem (:ExtraTimeHalftime,extraTimeHalftime,"extraTime_halftime"));
	    		menuItems.add(new MenuItem (:Alarms,alarms));
    		}
    		var menu = new Menu (menuItems, model.scoreHome+ " : "+model.scoreGuest);
    		Ui.popView (Ui.SLIDE_IMMEDIATE);
        	Ui.pushView(menu, new EditMenuDelegate(menu), WatchUi.SLIDE_IMMEDIATE);
    	}else if ( item.id == :Alarms){
    	    var menuItems=new [0];
    	     if(Attention has :playTone){
				menuItems.add(new StateMenuItem (:Beep,bip,"beep"));
			}
			if(Attention has :vibrate){
				menuItems.add(new StateMenuItem (:Vibrate,vibrate,"vibrate"));
			}
		
		    var menu = new Menu (menuItems, alarms);
            Ui.pushView(menu,new AlarmMenuDelegate(menu) ,  Ui.SLIDE_LEFT );
    	}
    }
    
    function onBack(){
    	model.newMatch();
		Ui.popView (Ui.SLIDE_LEFT);
		return true;
	}
}